package rest.app;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FileListingController {
    @RequestMapping("/container")
    public ContainerDirectoryListing fileListing(@RequestParam(value="location", defaultValue="containerFolder") String location) {
        return new ContainerDirectoryListing(location);
    }

}
