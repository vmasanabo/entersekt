package rest.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Component
public class HealthMonitor {
    private static final Logger logger = LoggerFactory.getLogger(HealthMonitor.class);

    @Scheduled(cron = "0 0 0/1 1/1 * ?")
    public void scheduleFixedRateWithInitialDelayTask() {
        final String uri = "http://localhost:8080/container";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);

        if(result.getStatusCode()==HttpStatus.OK){
            logger.info("File listing resting is still OK");
        }
        else{
            logger.error("Error in retrieving health service");
        }

        System.out.println(result.getStatusCode());
    }

}
