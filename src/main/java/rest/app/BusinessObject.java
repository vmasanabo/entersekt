package rest.app;


public class BusinessObject {
    private String filePath;
    private String fileTimeStamp;
    private String fileSize;
    private String isDirectory;
    private String isRegularFile;
    private String isSymbolic;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    public String getFileTimeStamp() {
        return fileTimeStamp;
    }

    public void setFileTimeStamp(String fileTimeStamp) {
        this.fileTimeStamp = fileTimeStamp;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getDirectory() {
        return isDirectory;
    }

    public void setDirectory(String directory) {
        isDirectory = directory;
    }

    public String getRegularFile() {
        return isRegularFile;
    }

    public void setRegularFile(String regularFile) {
        isRegularFile = regularFile;
    }

    public String getSymbolic() {
        return isSymbolic;
    }

    public void setSymbolic(String symbolic) {
        isSymbolic = symbolic;
    }
}
