package rest.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.stream.Stream;

public class FileListing {
    private static final Logger logger = LoggerFactory.getLogger(FileListing.class);
    private static int dirSize = 0;
    private static final int MAX = 1000000;
    private static BasicFileAttributes attr = null;
    private static StringBuilder buildString = new StringBuilder();
    private static ArrayList<BusinessObject> builderArrayList = new ArrayList<BusinessObject>();
    static BusinessObject businessObject = new BusinessObject();

    public FileListing() {
    }

    public static ArrayList<BusinessObject> containerDirectoryListingInfo(String location) throws IOException {

        logger.debug("Start  containerDirectoryListingInfo() for location : " + location);

        Stream<Path> files = null;
        Path path = Paths.get(location);

        logger.info("Get Path : " + path.toAbsolutePath());
        files = Files.walk(path);

        files.filter(p -> readAttributes(p) != null)
                .filter(p -> MAX > dirSize )
                .forEach(FileListing::processPath);

        logger.debug("Directory Size" + dirSize);
        files.close();
        logger.debug("Exit  containerDirectoryListingInfo() ");
        return builderArrayList;
    }

    private static BasicFileAttributes readAttributes(Path path) {
        try {
            attr = Files.readAttributes(path, BasicFileAttributes.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return attr;
    }

    private static void processPath(Path k) {
        businessObject.setFilePath(k.toAbsolutePath().toString());
        BasicFileAttributes attr = readAttributes(k);
        businessObject.setFileTimeStamp(attr.creationTime().toString());
        businessObject.setFileSize(String.valueOf(attr.size()));
        businessObject.setDirectory(String.valueOf(attr.isDirectory()));
        businessObject.setRegularFile(String.valueOf(attr.isRegularFile()));
        businessObject.setSymbolic(String.valueOf(attr.isSymbolicLink()));

        dirSize += attr.size();
        if (dirSize >= MAX) {
            logger.info("dirSize" + dirSize + "MAX" + MAX);
            logger.error("Directory returns more than 1 000 000 entries");

        }
        builderArrayList.add(businessObject);
    }
}