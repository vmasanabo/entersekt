package rest.app;

import java.io.IOException;
import java.util.ArrayList;

public class ContainerDirectoryListing {
    private String path;
    private ArrayList<BusinessObject> containerDirectory;

    public void setContainerDirectory() {
        this.containerDirectory = containerDirectory;
    }

    public ContainerDirectoryListing(String path){
        this.path = path;
    }

    public ArrayList<BusinessObject> getContainerDirectory() {
        try
        {
            containerDirectory  = FileListing.containerDirectoryListingInfo(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return containerDirectory;
    }

}
