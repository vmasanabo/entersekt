import rest.app.BusinessObject;
import rest.app.ContainerDirectoryListing;


public class TestNG {
    public static void main(String[] args) {
        TestNG.getAbsolutePath();
    }

    public static void getAbsolutePath() {

        ContainerDirectoryListing containerDirectoryListing = new ContainerDirectoryListing("containerFolder");
        containerDirectoryListing.getContainerDirectory().forEach(BusinessObject::getFileSize);

    }

}
