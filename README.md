1. Install docker.
  1.1 sudo apt-get install docker.
  
2. Check out code from https://vmasanabo@bitbucket.org/vmasanabo/entersekt.git
3. Change directory to the checked code.
4. Run:
sudo ./mvnw install dockerfile:build
sudo docker run -p 8080:8080 -t springio/gs-spring-boot-docker

5. Rest service is configured to run on port 8080.
   To run it all you have to do is to go to the browser and insert the following:
   http://localhost:8080/container?location=.
   